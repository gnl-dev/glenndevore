---
title: "Glenn DeVore README (GTM, Product Marketing, Expansion, Growth, Strategic Planning)"
description: "Learn more about working with Glenn DeVore"
---







# Glenn DeVore's README
👋, I’m Glenn.
 
This page is intended to help others understand what it might be like to work with me, especially for those who haven’t worked with me before.

## About Me
- I am a California native, born and raised in a small NorCal town, lived on the central coast as a young adult, then all around the Bay Area, settling a few years ago in the small north bay town of Mill Valley, with my wife, three kids, and dog.
- While I love to travel the world and experience new places and cultures, I always come back to my California home.
- I am a father of three beautifully wild (and sometimes feral) young children, who I do my best to keep alive and thriving.
- Favorite pastimes include reading (mainly audible), hiking (with my dog), vacationing (with family), and making chocolate.

### A Bit About My Career Journey Thus Far
I am deeply passionate about helping technical product-led companies uncover the core needs and motivations of users and buyers, helping organizations grow and expand their business.  I have been doing this work for over twenty years; thirteen of those leading and coaching teams.
 
My early career started in the dot-com boom, building websites, databases, and networks for small companies – later helping a small Internet Service Provider (ISP) grow and eventually sell to EarthLink.  After graduating from UC Berkeley, I helped bigger companies (Microsoft, Google, Apple, and others) build and launch new businesses and products as a management consultant at Accenture.  While consulting was only intended to last for a couple years, I continued to have a lot of fun (ideating on dozens of product launches, building an innovation lab, authoring new product positioning for expansion), and suddenly almost ten years went by.  I then spent a little over four years at Cisco helping turn around the Collaboration business unit, setting and driving the long-term strategic plans, repositioning WebEx, and helping launch new software and hardware.  My next adventure led me to AWS where I helped accelerate hyper-growth, leading Go-to-Market (GTM) for the Training & Certification business. This first role at AWS was a phenomenal learning experience in hyper-scaling.  The learnings were unmatched until my latest role, which all started with the writing of a PRFAQ that got a lot of attention…
 
… to be continued.

### What I do at AWS
My latest adventure at AWS started with helping author a vision document (referred to at Amazon as a PRFAQ – a press release, envisioned to be published in the future once the product launches).  After surprisingly getting funded by the CEO, I transitioned roles to lead the GTM for the launch of this new venture within AWS, that is now known as [AWS Cloud Institute](http://www.awscloudinstitute.com/) (ACI). For almost two years, I have led and overseen the creation and implementation of all GTM and Marketing initiatives for ACI: authoring the overarching GTM plan, conducting market and competitive research, defining initial target personas, formalizing the product pricing, defining product messaging, and managing early funnel conversion.

## My Leadership Philosophy
- Leading others is a service to perform, not a declaration of authority.
- I have incredibly high standards for myself and for my team.  Sometimes these standards can be seen as unreasonably high, and I work to tailor my feedback based on individualized needs, ensuring it is constructive and empowering.

### Some Coaching Principles I Aim to Live by
- Listen first, ask questions second, consider different angles, and give advice only when asked for it directly.
- Praise when praise is due; make it public whenever appropriate; make it proximate; negative feedback is always in private.
- Forced constraints in terms of frameworks are helpful aids, but also remember to think beyond them.


## Strengths and Weaknesses
### Personality Tests
- **CliftonStrengths:** [Top](https://drive.google.com/file/d/1z8C803Xt1p5licGqgFvVI6hCh29gMqdr/view?usp=drive_link) - Intellection, Learner, Strategic, Analytical, Responsibility
- **Myers-Briggs:** [INTP](https://drive.google.com/file/d/1mK-ToZNw9mioUKEEDQKT2f_gfUTLsCLN/view?usp=drive_link) “Logician” - Introverted, Intuitive, Thinking, Prospecting
- **Enneagram:** [Type 6](https://drive.google.com/file/d/1HRZU_20hcfdmf2Q4n-_1EbCYWMrbK2x-/view?usp=drive_link) “Loyal Skeptic”
- **Principles:** [Strategist; Shaper; Quiet Leader](https://drive.google.com/file/d/1WFOHnJ149RcuK5YdQEp4180YSE-aKCGu/view?usp=drive_link)

### Strengths
(What I am good at, and how that translates into tactical skills) 
- **Thoughtful:** Emphasizing careful reflection and meaningful dialogue, I value depth in understanding and interaction.
    - Crafting and refining narratives and presentations.
    - Tailoring messaging to resonate with varied audiences.
- **Highly analytical:** I delve into underlying reasons and causes, adept at considering various factors influencing scenarios.
    - Applying game theory and scenario analysis.
    - Identifying untapped market profitability opportunities.
    - Conducting thorough quantitative data modeling.
- **Life-Long-Learner:** Driven by a relentless pursuit of knowledge, I thrive on the learning journey itself.
    - Leading research initiatives and facilitating focus groups.
    - Ideating innovative concepts and developing solutions.
    - Rapidly assimilating new knowledge from diverse sources.
- **Strategic:** Skilled at discerning patterns and simplifying complex issues to forge clear paths forward.
    - Tailoring strategies to segmented customer needs.
    - Transforming obstacles into strategic advantages.
- **Responsible:** Committed to accountability and integrity, I ensure my actions reflect my values.
    - Ensuring strategic plans are executed with precision.
    - Prioritizing action items to motivate and drive results.

### Weaknesses
(Opportunities where I can improve)
- **Skeptical:** I tend to be cautious, focusing on identifying and addressing risks, which can sometimes be seen as excessive skepticism or negativity. My tendency to ask many questions upfront might unintentionally appear resistant. To address this, I emphasize that my questions stem from curiosity, and once I understand the rationale, I am fully committed.
- **Balancing Humble Confidence:** Striking the right balance between confidence and humility is an ongoing challenge for me. At my best, I recalibrate my approach to maintain humble confidence, ensuring my perspective is neither arrogant nor overly self-doubting.
- **Emotional Influence:** I tend to emphasize data and logic in my arguments, sometimes failing to acknowledge the crucial role of emotions and values. I am working on integrating more empathetic understanding and emotional insights into my interactions to enrich my communication and influence.
- **Critical:** My desire to uphold high standards can inadvertently overwhelm or discourage, especially when providing feedback. This especially applies when critiquing and caveating my own work.  I'm learning to tailor my feedback based on what is helpful for the moment, ensuring it is constructive and empowering without compromising high standards.
- **Pronunciations:** Navigating pronunciations can be tricky for me, especially with words learned through reading. Lately, I've been proactively seeking pronunciation guides online to improve, yet it remains an area requiring ongoing attention.


## Working Together
- **Communication:** At work, Slack is the best way to reach me (you can expect < 10min SLA 90% of the time).  Please tag me if it is urgent.  Outside of work, text messages are the most direct line (< 60min SLA 90% of the time).  Email (work or personal) also works; however, expect a 1–2-day SLA for most responses. 
- **Sync vs. Async:** I appreciate both for different contexts.  I prefer async for most quick questions or updates (facts, figures, FYI notes, project updates).  I prefer sync for deeper discussions, 1:1s, and ideation sessions (after solo time to think and write). 
- **Time of Day:** I am an early riser (~5am), but I also guard my mornings diligently (meditation, exercise, feeding kids, and school drop-off).  On most days, reaching me between 8:30 am and 5:30 pm PT is likely to be most successful.  I am also generally protective of my evenings (making and eating dinner with the family), but I can still be reached when urgent (Slack or text me).
- **Scheduling Meetings:** I like to timebox my calendar with action items to focus on and tasks to get done, mixed with the plethora of meetings.  This means that my calendar can get quite full.  However, some of it is fungible.  Please reach out (Slack) if you need time with me but can’t find it on my calendar.

### I Love it When People...
- Come prepared to a discussion and bring logical, deep, and critical thinking to the table.
- Express consistent civility – more than just treating others respectfully; acting to make others feel respected.
- Go out of their way to connect people where there is potential benefit to those being introduced.
- Say “I don’t know” when it’s the truth, even if it’s embarrassing, and then follow up after figuring it out.
- Proactively call themselves out when they mess up; take immediate responsibility and explain how they will rectify it.
- Support others in the room who voice a diverse opinion, even if the idea is strange at first; always encourage contribution.
- Give their full attention and focus to the task at hand – especially when in a meeting or providing feedback.

### I Can Live Without...
- Personalizing the impersonal or impersonalizing the personal.
- Hidden agendas, motivated by political gain and Machiavellian maneuvering for personal “power.”
- Hearing someone speak negatively about others, when they wouldn’t feel comfortable saying it to the person directly.
- Willful indifference to facts or reason and the willing dismissal of opportunities to learn.
- Contradictory instruction or advice (direction is always valued; misdirection is only for magicians).
- The phrase, “I just wanted to…” – Why “just?”  And if you “want-ED” to, does that mean you no longer do?

### Values I Greatly Appreciate in Others
- Integrity – say what you mean and do what you say.
- Respectful candor – straightforward with prompt and honest feedback that is constructive and kind.
- Leading with curiosity instead of authority – progress the idea, instead of “winning” the argument.
- Decisive humility – making high-velocity decisions while remaining humble in opinions.
- Trust in positive intent – and when misalignment arises (because it will), work to understand the root of misalignment.


## Outside of Work
At home, I mainly answer to the title of “Papa” to my three kids (two older boys and younger girl).  I read a lot of books (mostly audible) across  a wide range of interests in various topics: management, leadership, philosophy, psychology, neuroscience, astrophysics, quantum physics, …the list is long. One of my favorite pastimes is hiking around Marin (best expressed in German as “Waldeinsamkeit” for “solitude of the forest” or the joy one gets from wandering in nature).  One of my most recent hobbies is learning as much as I can about generative AI, and structuring projects for my kids to do the same.  Occasionally, I also enjoy making chocolate (bars and truffles) from scratch (cacao beans, butter, and sugar).


## Thank you for reading
Now that you know a bit more about me, I hope this helps us get to know each other further.  As you get to know me better, please let me know if there is anything that should either be added or edited into the content above.  Also, if you see any way in which I could better adapt to situations or other working styles, I gladly welcome and appreciate the feedback.